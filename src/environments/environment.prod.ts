export const environment = {
  production: true,
  urlBreedsBase:'https://dog.ceo/api',
  urlBreedsList:'/breeds/list/all',
  urlBreedsImages:'/breed/img/images'  
};
