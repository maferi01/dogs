import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core';
import { ErrorService } from 'src/app/services/error.service';
import { HttpErrorResponse } from '@angular/common/http';
/**
 * Base class for all components, It contains the common properties and functions
 */
export abstract class BaseComponent implements OnDestroy{
    // set here common items of component
    protected subs: Subscription[]=[];

    constructor(protected errorServ:ErrorService){

    }

    /**
     * Unsiscribes all subscriptions
     */
    ngOnDestroy(): void {
        this.subs.forEach(s=>s.unsubscribe());
    }

    /**
     * Conrtrol error from Component, sends to service
     * @param er 
     */
    controlError(er: Error): void {
        console.error('Error base component',er);
        if(!(er instanceof HttpErrorResponse)){
            this.errorServ.sendError(er);
        }
        
      }
}

