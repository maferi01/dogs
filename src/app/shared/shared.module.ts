import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'primeng/button';
import { CarouselModule } from 'primeng/carousel';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';

/**
 * Shared modules contains the common components such as durectives, pipes, comps..
 */
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    CarouselModule,
    ButtonModule,
    DropdownModule,
    DialogModule,
    TranslateModule
  ],
  exports: [
    CarouselModule,
    ButtonModule,
    DropdownModule,
    DialogModule
  ]
})
export class SharedModule { }
