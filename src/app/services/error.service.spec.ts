import { TestBed } from '@angular/core/testing';
import { RootModule } from '../root/root.module';
import { ServicesModule } from './services.module';
import { ErrorService } from './error.service';


describe('ErrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[ 
      
      ],
      providers:[ErrorService]
  }));

  it('should be created error service ', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    expect(service).toBeTruthy();
  });


  it('should init subject ', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    expect(service.subject).toBeTruthy();
  });



  it('should send error call next', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    spyOn(service.subject,'next');
    service.sendError('error test');
    expect(service.subject.next).toHaveBeenCalled();
  });


  it('should send error to subject and suscribe', () => {
    const service: ErrorService = TestBed.get(ErrorService);
    let result;
    service.subject.subscribe(d=>result=d);
    service.sendError('error test');
    expect(result).toBeTruthy();
    expect(result).toBe('error test');
  });
  
});
