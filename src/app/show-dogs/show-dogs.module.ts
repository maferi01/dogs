import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { DogsDataService } from './services/dogs-data.service';
import { ShowPetsRoutingModule as ShowDogsRoutingModule } from './show-dogs-routing.module';
import { HomeDogsComponent } from './pages/home-dogs/home-dogs.component';
import { FilterDogsComponent } from './components/filter-dogs/filter-dogs.component';
import { ListDogsComponent } from './components/list-dogs/list-dogs.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

/**
 * Dogs module contains all the pages and features of this functional logic.
 */

@NgModule({
  declarations: [
  HomeDogsComponent,
  FilterDogsComponent,
  ListDogsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    ShowDogsRoutingModule,
    TranslateModule.forChild({})
    
  ],
  providers: [DogsDataService]
})
export class ShowDogsModule { }
