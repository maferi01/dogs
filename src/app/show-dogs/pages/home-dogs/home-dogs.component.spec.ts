import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { HomeDogsComponent } from './home-dogs.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FilterDogsComponent } from '../../components/filter-dogs/filter-dogs.component';
import { ListDogsComponent } from '../../components/list-dogs/list-dogs.component';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DogsDataService } from '../../services/dogs-data.service';
import { ServicesModule } from 'src/app/services/services.module';
import { of } from 'rxjs';
import { IDogBreeds } from '../../services/models-dogs';
import { delay, map } from 'rxjs/operators';
import { getTestScheduler, cold } from 'jasmine-marbles';

describe('HomeDogsComponent', () => {
  let component: HomeDogsComponent;
  let fixture: ComponentFixture<HomeDogsComponent>;
  let mockDogsDataService:jasmine.SpyObj<DogsDataService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ 
        ServicesModule,
        TranslateModule.forRoot(),
        ReactiveFormsModule,
        FormsModule,
        SharedModule
        ],
      providers: [
        {
          provide: DogsDataService,
          useValue: jasmine.createSpyObj('DogsDataService', ['getDogBreedsList','getDogsList'])
        },
        ],   
      declarations: [HomeDogsComponent,FilterDogsComponent,ListDogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    mockDogsDataService= TestBed.get(DogsDataService);
    mockDogsDataService.getDogBreedsList.and.returnValue(of([]))
    const dogBreedsMock: IDogBreeds[]=[{name:'m',types:[]},{name:'y',types:[]}];
    mockDogsDataService.getDogBreedsList.and.returnValue(of(dogBreedsMock).pipe(map(d=>d)))
    mockDogsDataService.getDogsList.and.returnValue(of(['image1','image2']).pipe(map(d=>d)))
    
    fixture = TestBed.createComponent(HomeDogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create HomeDog', () => {
    expect(component).toBeTruthy();
  });

  it('should set valid array', async(() => {
    getTestScheduler().flush(); 
    fixture.detectChanges();
  
    console.log('breedss*********',component.dogBreeds);
    expect(mockDogsDataService.getDogBreedsList).toHaveBeenCalledTimes(1);
    expect(component.dogBreeds).toBeTruthy();
    expect(component.dogBreeds.length===2).toBeTruthy();
    
  }));

  it('should search call getDogsList', async(() => {
    component.search({breedSelect:{name:'a',types:[]},typeSelect:'type'});  
    getTestScheduler().flush(); 
    fixture.detectChanges();
    expect(mockDogsDataService.getDogsList).toHaveBeenCalled();
    expect(mockDogsDataService.getDogsList).toHaveBeenCalledTimes(1);
  }));

  it('should search set images', async(() => {
    component.search({breedSelect:{name:'a',types:[]},typeSelect:'type'});  
    getTestScheduler().flush(); 
    fixture.detectChanges();
    expect(component.dogImages.length).toBe(2);
  }));

  it('should create filter component', async(() => {
    getTestScheduler().flush(); 
    fixture.detectChanges();
    const filter = fixture.debugElement.nativeElement.querySelector('app-filter-dogs'); 
    console.log('filter ***',filter);
    expect(filter).toBeTruthy();
    
  }));

  it('should create list dog components after search', async(() => {
    component.search({breedSelect:{name:'a',types:[]},typeSelect:'type'});  
    getTestScheduler().flush(); 
    fixture.detectChanges();
    const filter = fixture.debugElement.nativeElement.querySelector('app-filter-dogs'); 
    const list = fixture.debugElement.nativeElement.querySelector('app-list-dogs'); 
    console.log('filter ***',filter);
    expect(filter).toBeTruthy();
    expect(list).toBeTruthy();
  }));



});
