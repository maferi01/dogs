import { Component, OnInit } from '@angular/core';
import { DogsDataService } from '../../services/dogs-data.service';
import { IDogBreeds, IDogBreedSelect } from '../../services/models-dogs';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { ErrorService } from 'src/app/services/error.service';
/**
 * This is the Container Component, it just contains the data logic, the service calls and
 * it contains the child presentation components, this container gets the child comps work well together.  
 * Manage the error if it happens
 */
@Component({
  selector: 'app-home-dogs',
  templateUrl: './home-dogs.component.html',
  styleUrls: ['./home-dogs.component.scss']
})
export class HomeDogsComponent extends BaseComponent implements OnInit {
  dogBreeds: IDogBreeds[];
  dogImages: string[];

  constructor(private dogsService: DogsDataService,protected errorServ:ErrorService) {super(errorServ)}

  //inits the view, gets the initial list and it binds the result to the presentation child
  ngOnInit() {
    const sub=this.dogsService.getDogBreedsList().subscribe(list => (this.dogBreeds = list),er=>this.controlError(er));
    this.subs.push(sub);
  }

  /**
   * Search the dog pictures and sets the result to the presentation child
   */
  search(selected: IDogBreedSelect) {
    const sub=this.dogsService.getDogsList(selected).
    subscribe(list=>this.dogImages=list,er=>this.controlError(er));
    this.subs.push(sub);
  }
}
