import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { ErrorService } from 'src/app/services/error.service';


/**
 * Presentation component to list dog imges, it supports different views, normal and carrusel with zoom dialog
 * It is responsive presentation
 * ONPUSH component
 */
@Component({
  selector: 'app-list-dogs',
  templateUrl: './list-dogs.component.html',
  styleUrls: ['./list-dogs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush 
})
export class ListDogsComponent extends BaseComponent implements OnInit {

  @Input()
  imagesDog:string[];
  view:'normal'|'carrusel';
  imageSelec:string;

  constructor(protected errorServ:ErrorService) { 
    super(errorServ);
    this.view='normal';
  }

  ngOnInit() {
  }


}
