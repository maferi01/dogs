import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { ListDogsComponent } from './list-dogs.component';
import { ServicesModule } from 'src/app/services/services.module';
import { By } from '@angular/platform-browser';

describe('ListDogsComponent', () => {
  let component: ListDogsComponent;
  let fixture: ComponentFixture<ListDogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ServicesModule,
        TranslateModule.forRoot(),
        SharedModule
      ],      
      declarations: [ 
        ListDogsComponent 
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDogsComponent);
    component = fixture.componentInstance;
    const imagesDog:string[]=['img1','img2','img3'];
    component.imagesDog=imagesDog;
    fixture.detectChanges();
  });

  it('should create ListDogs', () => {
    expect(component).toBeTruthy();
  });

  it('should show init normal view ', () => {
    const listdogs = fixture.debugElement.nativeElement.querySelector('.listdogs__normal'); 
    expect(listdogs).toBeTruthy();
  });

  it('should be disable button normal', () => {
    const butDisabled = fixture.debugElement.nativeElement.querySelector('.normal button[disabled]'); 
    expect(butDisabled).toBeTruthy();
  });
  
  it('should select carrusel view ', fakeAsync(() => {
    const butCarru=fixture.debugElement.query(By.css('.carrusel button')); 
    expect(butCarru).toBeTruthy();
    butCarru.triggerEventHandler('click',null);
    tick();
    fixture.detectChanges();
    const carrusel = fixture.debugElement.nativeElement.querySelector('.listdogs__img'); 
    expect(carrusel).toBeTruthy();
    const carruselImg = fixture.debugElement.nativeElement.querySelector('.listdogs__img__cont'); 
    expect(carruselImg).toBeTruthy();
  }));
});
