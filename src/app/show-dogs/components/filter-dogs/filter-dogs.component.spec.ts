import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FilterDogsComponent } from './filter-dogs.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ServicesModule } from 'src/app/services/services.module';
import { IDogBreedSelect } from '../../services/models-dogs';
import { getTestScheduler } from 'jasmine-marbles';

describe('FilterDogsComponent', () => { 
  let component: FilterDogsComponent;
  let fixture: ComponentFixture<FilterDogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        ServicesModule,
        SharedModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
       ],
      declarations: [ FilterDogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterDogsComponent);
    component = fixture.componentInstance;
    component.dogBreeds=[{name:'b1',types:[]},{name:'b2',types:['t1','t2']}]
    fixture.detectChanges();
  });

  it('should create OK', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid init form', () => {
    expect(component.filterForm.valid).toBeFalsy();
  });

  it('should be valid after select', () => {
    component.filterForm.controls[component.breed].setValue('b1');
    expect(component.filterForm.valid).toBeTruthy();
  });

  it('should select breedSelected', () => {
    component.filterForm.controls[component.breed].setValue('b2');
    fixture.detectChanges();
    expect(component.breedSelected).toBeTruthy();
    expect(component.breedSelected.name).toBe('b2');
  });

  it('should select breedSelected', () => {
    component.filterForm.controls[component.breed].setValue('b2');
    fixture.detectChanges();
    expect(component.breedSelected).toBeTruthy();
    expect(component.breedSelected.name).toBe('b2');
    expect(component.filterForm.controls[component.type].value).toBe('all');
  });

  it('should emit filter value', async(() => {
   // spyOn(component.filter,'emit');
    let valfilter:IDogBreedSelect;
    component.filter.subscribe(d=>valfilter=d); 
    component.filterForm.controls[component.breed].setValue('b2');
    fixture.detectChanges();
    expect(component.breedSelected).toBeTruthy();
    component.filterForm.controls[component.type].setValue('t2');
    fixture.detectChanges();
    component.search();
    getTestScheduler().flush(); 
    fixture.detectChanges();
    
    //expect(component.filter.emit).toHaveBeenCalled();
    expect(valfilter.breedSelect.name).toBe('b2');
    expect(valfilter.typeSelect).toBe('t2');
  }));

});
