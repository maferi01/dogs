import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { IDogBreeds, IDogBreedSelect } from '../../services/models-dogs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { map, tap, filter } from 'rxjs/operators';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { ErrorService } from 'src/app/services/error.service';

/**
 * Presentation/form component to filter the list dog imges, It contains the form which emits the filter params
 * ONPUSH component
 */
@Component({
  selector: 'app-filter-dogs',
  templateUrl: './filter-dogs.component.html',
  styleUrls: ['./filter-dogs.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class FilterDogsComponent extends BaseComponent implements OnInit {
  @Input()
  dogBreeds: IDogBreeds[];
  @Output()
  filter = new EventEmitter<IDogBreedSelect>();

  filterForm: FormGroup;
  breed = 'breed';
  type = 'type';
  breedSelected: IDogBreeds;
  typeSelected: string;

  /**
   * Creates the form builder and its fields
   * @param errorServ 
   */
  constructor(protected errorServ: ErrorService) {
    super(errorServ);
    this.filterForm = new FormGroup({
      [this.breed]: new FormControl(null,[Validators.required]),
      [this.type]: new FormControl({value:null,disabled:true})
    });
  }


  /**
   * Inits the reactive form, suscribes to change list and updates the form
   */
  ngOnInit() {
    
    const sub=this.filterForm.controls[this.breed].valueChanges
      .pipe(
        tap(val => console.log('breed************', val)), //debug  value slected
        tap(() => this.filterForm.controls[this.type].disable()), // disables the types list
        filter(val=>!!val),  // only valid values
        map(value => this.dogBreeds.find(br => br.name === value)), //get the selected model 
        tap((value: IDogBreeds) => (this.breedSelected = value)), //set the selected
        tap(() => this.filterForm.controls[this.type].setValue('all')), //set value all
        tap(() => { //enable/disable combo depending on current types
          if (!this.breedSelected.types || this.breedSelected.types.length === 0) {
            this.filterForm.controls[this.type].disable();
          } else {
            this.filterForm.controls[this.type].enable();
          }
        })
      )
      .subscribe();
      this.subs.push(sub);
  }

  /**
   * Emits the params filter
   */
  search() {
    this.filter.emit({
      breedSelect: this.breedSelected,
      typeSelect: this.filterForm.controls[this.type].value
    });
    this.filterForm.markAsPristine();
  }
  
}
