
/**
 * Models For dog Model
 */
/**
 * Request and respond models
 */
export interface IResponBreedDogs {
  message: IBreedDogsList;
  status: string;
}
export interface IBreedDogsList{
  [name:string]:string[];
}
export interface IResponDogs {
  message: string[];
  status: string;
}


/**
 * Generic Models for the view and services
 */
export interface IDogBreeds{
  name:string;
  types:string[];
}

export interface IDogBreedSelect{
  breedSelect:IDogBreeds;
  typeSelect:string;
}