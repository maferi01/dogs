import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { IResponBreedDogs, IDogBreeds, IDogBreedSelect, IResponDogs } from './models-dogs';
import { environment } from '../../../environments/environment';

/**
 * Service dogs implements all the logic required.
 */
@Injectable()
export class DogsDataService {
  //urls dog
  urlBreedsDog: string;
  urlDogImages: string;

/**
 * inits the url with environment conf
 * @param http , 
 */
    constructor(private http: HttpClient) {
    this.urlBreedsDog = environment.urlBreedsBase + environment.urlBreedsList;
    this.urlDogImages = environment.urlBreedsBase+ environment.urlBreedsImages;
  }

  /**
   * get list dog breeds from serve,
   */

  getDogBreedsList(): Observable<IDogBreeds[]> {
    return this.http.get(this.urlBreedsDog).pipe(
      tap(d => console.log('request list pets', d)),
      map((d: IResponBreedDogs) => {
        // convert Response to simple dog breeds model
        return Object.keys(d.message).map(key => {
          return {
            name: key,
            types: d.message[key]
          } as IDogBreeds;
        });
      }),
      tap(d => console.log('convert list pets', d))
    );
  }

  /**
   * get list dog images from serve,
   * @param filter param
   */
  getDogsList(filter: IDogBreedSelect): Observable<string[]> {
    const urlDogs=this.getUrlDogs(filter.breedSelect.name);
    return this.http.get(urlDogs).pipe(
      tap(d => console.log('request list dogs', d)), // debug
      map((d: IResponDogs) => d.message), //map convert
      map((d: string[]) => (filter.typeSelect && filter.typeSelect !== 'all' ? d.filter(cad => cad.includes(filter.typeSelect)) : d)), //filter images by type
      tap(d => console.log('convert list dogs', d)) // debug result
    );
  }
  /**
   * Helper to get the url with param
   * @param name 
   */
  getUrlDogs(name: string) {
    return this.urlDogImages.replace('/img/', `/${name}/`);
  }
}
