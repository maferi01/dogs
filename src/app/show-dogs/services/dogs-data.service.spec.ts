import { TestBed, async } from '@angular/core/testing';

import { DogsDataService } from './dogs-data.service';
import { HttpClient } from '@angular/common/http';
import { IResponBreedDogs, IResponDogs } from './models-dogs';
import { of, throwError } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

describe('DogsDataService', () => {
  let mockHttpService:jasmine.SpyObj<HttpClient>;
  mockHttpService=jasmine.createSpyObj('HttpClient', ['get']);
  beforeEach(() => TestBed.configureTestingModule({
    imports:[ 
      ],
     providers:[
      {
        provide: HttpClient,
        useValue: mockHttpService
      }, 
      DogsDataService]  
  }));

  it('should be created dog service ', () => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    expect(service).toBeTruthy();
  });

  it('should call http breeds ', () => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    const resp:IResponBreedDogs={
      message:{},
      status:'ok'
    };
    mockHttpService.get.and.returnValue(of(resp));
    service.getDogBreedsList();
    expect(mockHttpService.get).toHaveBeenCalled();
  });

  it('should call http breeds only 2 items', async(() => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    const resp:IResponBreedDogs={
      message:{
        breed1:[],
        breed2:[]
      },
      status:'ok'
    };
    mockHttpService.get.and.returnValue(of(resp)); 
    service.getDogBreedsList().pipe(
      delay(3000),
      tap(dat=>console.log('breeds total',dat)),
    ).subscribe(
      data=> expect(data.length).toBe(2)
    );
    
  }));

  it('should call http error', async(() => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    mockHttpService.get.and.returnValue(throwError ({error:'http'})); 
    service.getDogBreedsList().pipe(
      delay(3000),
      tap(dat=>console.log('breeds total',dat)),
    ).subscribe(
      data=> expect(data).toBeUndefined(),
      er=> expect(er).toBeDefined()
    );
    
  }));

  it('should call http dog images', async(() => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    const resp:IResponDogs={message:['img1','img2'],status:'ok'}
    mockHttpService.get.and.returnValue(of(resp)); 
    service.getDogsList({breedSelect:{name:'br',types:[]},typeSelect:'all'}).pipe(
      delay(3000),
      tap(dat=>console.log('images total',dat)),
    ).subscribe(
      data=> expect(data.length).toBe(2),
      er=> expect(er).toBeUndefined()
    );
    
  }));

  it('should call http dog images with type', async(() => {
    const service: DogsDataService = TestBed.get(DogsDataService);
    const resp:IResponDogs={message:['img1','img2'],status:'ok'}
    mockHttpService.get.and.returnValue(of(resp)); 
    service.getDogsList({breedSelect:{name:'br',types:[]},typeSelect:'typea'}).pipe(
      delay(3000),
      tap(dat=>console.log('images total',dat)),
    ).subscribe(
      data=> expect(data.length).toBe(0),
      er=> expect(er).toBeUndefined()
    );
    
  }));
});
