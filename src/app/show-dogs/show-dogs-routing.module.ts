import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeDogsComponent } from './pages/home-dogs/home-dogs.component';

const routes: Routes = [
  {
    path: '',
    component: HomeDogsComponent},
];

@NgModule({
 imports: [RouterModule.forChild(routes)],
 exports: [RouterModule]
})
export class ShowPetsRoutingModule { }
