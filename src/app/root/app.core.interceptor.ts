import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { ErrorService } from '../services/error.service';

/**
 * Interceptor to control http errors.
 */
@Injectable()
export class AppErrorHttpInterceptor  implements HttpInterceptor {
  constructor(private errorService: ErrorService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

          return next.handle(req)
          .pipe(
           catchError((err) => {
            console.error('intercept error ==' ,err);
            //manage common errors http, sends to service and propagate error
            this.errorService.sendError(err);
            return throwError(err);
          })
        );

        }

}
