import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutRouterComponent } from './components/layout-router/layout-router.component';
import { HeaderComponent } from './components/header/header.component';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AppErrorHttpInterceptor } from './app.core.interceptor';

/**
 * Root module for this app, It lives always, and loads persistent modules and main component 
 */

 
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [AppComponent, LayoutRouterComponent, HeaderComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
  })
  ],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppErrorHttpInterceptor,
      multi: true
    }],
  exports: [AppComponent]
})
export class RootModule {
  /**
   * Inits the root, sets the translate properties
   * @param translate 
   */
  constructor(private translate: TranslateService){
    this.translate.addLangs(["en","sp","fr"]);
    this.translate.setDefaultLang('en');

  }
  
   }
