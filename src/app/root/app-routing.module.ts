import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowDogsModule } from '../show-dogs/show-dogs.module';

/**
 * Router module and configuration with lazy loading load
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: '/dogs' ,
    pathMatch: 'full'
  },
  {
    path: 'dogs',
    loadChildren: () => ShowDogsModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
