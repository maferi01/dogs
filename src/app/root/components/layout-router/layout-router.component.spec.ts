import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServicesModule } from 'src/app/services/services.module';

import { RootModule } from '../../root.module';
import { LayoutRouterComponent } from './layout-router.component';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from '../header/header.component';
import { AppRoutingModule } from '../../app-routing.module';

xdescribe('LayoutRouterComponent', () => { 
  let component: LayoutRouterComponent;
  let fixture: ComponentFixture<LayoutRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ 
 //       AppRoutingModule,
        TranslateModule, 
        ServicesModule
        ],
      declarations: [ LayoutRouterComponent,HeaderComponent
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create layout ***', () => {
    expect(component).toBeTruthy();
  });
});
