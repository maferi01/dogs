import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { ErrorService } from 'src/app/services/error.service';
/**
 * Header component , it has the title, logo .......
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent implements OnInit {

  constructor(private translate: TranslateService,protected errorServ:ErrorService) {
    super(errorServ);
   }

  ngOnInit() {
  }
  /**
   * Set current language
   * @param ln 
   */
  setLn(ln:string){
    this.translate.setDefaultLang(ln);
  }
}
