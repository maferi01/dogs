import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { RootModule } from './root/root.module';
import { ServicesModule } from './services/services.module';
import { AppComponent } from './root/app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
/**
 * Module base of this app, It begins the main structure module
 */
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RootModule,
    ServicesModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
