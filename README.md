# Dogs

# Important notes:

I want to remark some points of this project that I apply in order you can understand better my approach:

--------------------------------
# Feature module pattern
 
 however this is a small project and this structure pattern is not necessary I want to use it because it allows distribute the projects elements in proper way. Also the project maintain-extension  is much better and easier, and fix very well for lazy-loading .
   the structure is : 
    
    root module-> it contains elements that we want to load from beggining(router, header-footer,interceptors ....). Normally it is deployed in the bundle main.js. it is loaded at first.

    services module-> it contains the common services.

    shared module->  it contains the common components.

    feature function module-> It encapsulates the one entity functions (dogs module). I load this module with lazy-loading, when the router requires it. This way improves the bundles and its loading performance.  

    ....more modules.... in this level, for instance f we would want to add a birds module...

-----------------------

# Container-presentation pattern

 with this pattern we separate the presentation and DOM-managment from data logic, between componentes. in this way we control much better the workflow between them . And it allows us to reuse the component in easier way.

----------------------------------------------------------------------------------------

# Interceptor pattern

I use this powerful pattern to extend the project transparently. In this case to control http errors in simple way.

-------

# Good look-feel
 In this case use the library NG-Prime to improve the look-feel, I use carousel modal or normal list to show the images list.
 I could have aplied other interesting ways to show the list such as Pagination, Infinity Scroll(add elements to list), or Virtual scroll(improves the render DOM, adds and removes elements) proper for too big list,
 I could use other libraries such as Material, JQWidget.. Or use our own component library . I also use CSS3 and scss to style the presentation .
_____________________________________________________________

# Support Internatialization-languajes
, I use the angular library:

@ngx-translate/core , It offers good support for this. In this project yo can choose between Spanish, English.

----------------------------------------------------------------------------------------------------------
# Test project
 , I use Karma and api jasmine to test services and components . to test the project:

ng test , for coverrage-> ng test --code-coverage

-----------------------------------------------------------------------------------------------------

# Support environment
, I use the Angular built-function working with the files environement.ts, we can define the properties for every system.
to execute envoronment  :

 ng build --configuration=production
 
 ng build --configuration=pre

-------------------------------------------------------------------------------------------------------
# Use Form reactive
, Reactive programming is suitable to control complicate tasks and asyncronus functions, for forms we can control the fields status much better. and test them.

----------------------------------------------------------------------------------------------------------------

# Control data structures 
, I use type models to manage the structures,  It allows us to know which data are treating in every moment.

-------------------------------------------------------------------------------

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
